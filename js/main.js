/*Теоретичні питання та відповіді.
    Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.


/*Завдання
Дано масив books.

Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
Примітка
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Література:
Перехоплення помилок, "try..catch"
try...catch на MDN */


const books = [
    {
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70,
    },
    {
      author: "Сюзанна Кларк",
      name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
      name: "Дизайн. Книга для недизайнерів.",
      price: 70,
    },
    {
      author: "Алан Мур",
      name: "Неономікон",
      price: 70,
    },
    {
      author: "Террі Пратчетт",
      name: "Рухомі картинки",
      price: 40,
    },
    {
      author: "Анґус Гайленд",
      name: "Коти в мистецтві",
    },
  ];
  
  const rootElement = document.getElementById("root");
  
  function createBookElement(book) {
    const listItem = document.createElement("li");
    listItem.textContent = `Author: ${book.author}, Name: ${book.name}, Price: ${book.price || "Unknown"}`;
    return listItem;
  }
  
  function displayBooks() {
    const ul = document.createElement("ul");
  
    books.forEach((book) => {
      try {
        if (!("author" in book)) {
          throw new Error("Missing 'author' property");
        }
        if (!("name" in book)) {
          throw new Error("Missing 'name' property");
        }
        if (!("price" in book)) {
          throw new Error("Missing 'price' property");
        }
  
        const bookElement = createBookElement(book);
        ul.appendChild(bookElement);
      } catch (error) {
        console.error(`Invalid book: ${error.message}`);
      }
    });
  
    rootElement.appendChild(ul);
  }
  
  displayBooks();


